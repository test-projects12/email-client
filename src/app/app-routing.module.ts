import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AdminLayoutComponent } from './admin/share/components/admin-layout/admin-layout.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { MailsPageComponent } from './pages/mails-page/mails-page.component';
import { MainLayoutComponent } from './share/components/main-layout/main-layout.component';
import { AngularFireAuthGuard, canActivate, hasCustomClaim, redirectLoggedInTo, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { MailShowPageComponent } from './pages/mail-show-page/mail-show-page.component';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);
const redirectLoggedInToMails = () => redirectLoggedInTo(['']);
const adminOnly = () => hasCustomClaim('admin');

const routes: Routes = [
  {
    path: '', component: MainLayoutComponent, children: [
      { path: '', redirectTo: '/', pathMatch: 'full' },
      {
        path: '',
        component: MailsPageComponent,
        canActivate: [AngularFireAuthGuard],
        ...canActivate(redirectUnauthorizedToLogin)
      },
      {
        path: 'show/:id', component: MailShowPageComponent,
        canActivate: [AngularFireAuthGuard],
        ...canActivate(redirectUnauthorizedToLogin)
      },
      {
        path: 'admin',
        component: AdminLayoutComponent,
        canActivate: [AngularFireAuthGuard],
        ...canActivate(adminOnly)
      }
    ]
  },
  {
    path: 'login',
    component: LoginPageComponent,
    canActivate: [AngularFireAuthGuard],
    ...canActivate(redirectLoggedInToMails)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
