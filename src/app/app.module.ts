import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { AppComponent } from './app.component';

import { BreadcrumbModule as NgBreadcrumbModule } from 'angular-crumbs';
import { MenubarModule } from 'primeng/menubar';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { SidebarModule } from 'primeng/sidebar';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { ToolbarModule } from 'primeng/toolbar';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DialogService, DynamicDialogModule } from 'primeng/dynamicdialog';
import { InputTextModule } from 'primeng/inputtext';
import { EditorModule } from 'primeng/editor';
import { CardModule } from 'primeng/card';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ChipsModule } from 'primeng/chips';
import { ToggleButtonModule } from 'primeng/togglebutton';

import { AdminLayoutComponent } from './admin/share/components/admin-layout/admin-layout.component';
import { MainLayoutComponent } from './share/components/main-layout/main-layout.component';
import { MailsPageComponent } from './pages/mails-page/mails-page.component';
import { MailShowPageComponent } from './pages/mail-show-page/mail-show-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { ApiService } from './share/services/api/api.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { CreateMailComponent } from './pages/mails-page/modal/create-mail.component';
import { environment } from 'src/environments/environment';
import { BeEmulationService } from './share/services/api/be-emulation.service';
import { AuthService } from './share/services/api/auth.service';
import { UidToEmailPipe } from './share/pipes/uid-to-email.pipe';
import { MenuService } from './share/services/menu/menu.service';

const ngPrimeModules = [
  MenubarModule,
  BreadcrumbModule,
  SidebarModule,
  TableModule,
  ToggleButtonModule,
  ButtonModule,
  ToolbarModule,
  ConfirmDialogModule,
  DynamicDialogModule,
  InputTextModule,
  CardModule,
  EditorModule,
  MessagesModule,
  MessageModule,
  ChipsModule
];

@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    MailsPageComponent,
    MailShowPageComponent,
    LoginPageComponent,
    AdminLayoutComponent,
    CreateMailComponent,
    UidToEmailPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule.enablePersistence(),
    ...ngPrimeModules,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgBreadcrumbModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    DialogService,
    ConfirmationService,
    BeEmulationService,
    AuthService,
    ApiService,
    MessageService,
    MenuService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
