import { AuthService } from './../../services/api/auth.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Breadcrumb, BreadcrumbService } from 'angular-crumbs';
import { tap } from 'rxjs/operators';
import { MenuItem } from 'primeng/api';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Router } from '@angular/router';
import { MenuService } from '../../services/menu/menu.service';

@UntilDestroy()
@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.sass']
})
export class MainLayoutComponent implements OnInit {
  menuItems: MenuItem[];
  breadcrumbsItems: MenuItem[];
  home: MenuItem;
  displaySideBar = false;
  type = 'Inbox';

  constructor(
    private breadcrumbService: BreadcrumbService,
    private authService: AuthService,
    private router: Router,
    private menuService: MenuService,
  ) {
      this.home = {icon: 'pi pi-home', routerLink: '/'};
      this.menuItems = [
        {
          icon: 'pi pi-fw pi-align-justify',
          command: () => {
            this.displaySideBar = true;
          }
        }];
    }

  ngOnInit(): void {
    this.breadcrumbService.breadcrumbChanged
      .pipe(
        tap((crumbs) => {
          this.breadcrumbsItems =  crumbs.map(c => this.toPrimeNgMenuItem(c));
        }),
        untilDestroyed(this)
      ).subscribe();
  }

  logout(): void {
    this.authService.logout().pipe(
      tap(() => {
        this.router.navigate(['/login']);
      }),
      untilDestroyed(this)
    ).subscribe();
  }

  select(type): void {
    this.type = type;
    this.menuService.type$.next(type);
    this.displaySideBar = false;
  }

  private toPrimeNgMenuItem(crumb: Breadcrumb): MenuItem {
    // console.log(crumb.displayName, `#${crumb.url}`);
    return { label: crumb.displayName, url: `#${crumb.url}`} as MenuItem;
  }

}
