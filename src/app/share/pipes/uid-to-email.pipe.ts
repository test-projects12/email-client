import { Pipe, PipeTransform } from '@angular/core';
import { BeEmulationService } from '../services/api/be-emulation.service';

@Pipe({
  name: 'uidToEmail'
})
export class UidToEmailPipe implements PipeTransform {

  constructor(private beEmulationService: BeEmulationService) {}

  transform(value: string): string {
    return this.beEmulationService.getEmailByUid(value);
  }

}
