export interface IBeSendingMail {
    id: string;
    favorite: boolean;
    sendTime: Date;
    owner: any;
    theme: string;
    wasWatched: boolean;
    type: EmailTypesEnum;
}

export enum EmailTypesEnum {
    Taken = 'taken',
    Sended = 'sended'
}
