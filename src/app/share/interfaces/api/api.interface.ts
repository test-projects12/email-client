import { EmailTypesEnum } from './be-emulation.interface';

export interface IPreviewMail {
    id: string;
    favorite: boolean;
    sendTime: any;
    owner: any;
    theme: string;
    wasWatched: boolean;
}

export interface IMail extends IPreviewMail {
    messageText: string;
    recipients: Array<any>;
    type: EmailTypesEnum;
}

export interface ISendingMail {
    messageText: string;
    recipients: Array<string>;
    theme: string;
}

export interface IUser {
    email: string;
    password: string;
}

export interface IFirebaseStatus {
    idToken: string;
    expiresIn: string;
}
