import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { MessageService } from 'primeng/api';
import { from, Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { IUser } from '../../interfaces/api/api.interface';

@UntilDestroy()
@Injectable()
export class AuthService {

    constructor(
        private auth: AngularFireAuth,
        private messageService: MessageService,
    ) { }

    login(user: IUser): Observable<boolean> {
        return from(this.auth.signInWithEmailAndPassword(user.email, user.password))
        .pipe(
            catchError(this.handleEror.bind(this))
        );
    }

    logout(): Observable<void> {
        return from(this.auth.signOut());
    }

    getMyId(): Observable<firebase.User> {
        return this.auth.user;
    }

    private handleEror(error: HttpErrorResponse): Observable<any> {
        const message = error.message;
        this.messageService.add({
            key: 'msg',
            severity: 'error',
            summary: 'Error',
            detail: message
        });
        return throwError(message);
    }

}
