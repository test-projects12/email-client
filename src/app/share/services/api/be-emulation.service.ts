import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { forkJoin, from, observable, Observable, of } from 'rxjs';
import { concatMap, map, switchMap, tap } from 'rxjs/operators';
import { IPreviewMail, ISendingMail } from '../../interfaces/api/api.interface';
import { EmailTypesEnum, IBeSendingMail as IBeSendingMail } from '../../interfaces/api/be-emulation.interface';
import { AuthService } from './auth.service';

@Injectable()
export class BeEmulationService {

    mailsCollectionName = 'mails';
    mailsCollection: AngularFirestoreCollection<IPreviewMail>;
    private uidAssociations = {
        kPGefDklo0UlKNy9mLDWATFCZ273: 'test@mail.ru',
        '2VK1YlMDVpZxlpCQH7ALpXxb4zA2': 'test2@mail.ru'
    };

    constructor(
        private firestore: AngularFirestore,
        private authService: AuthService,
    ) {
        this.mailsCollection = firestore.collection<IPreviewMail>(this.mailsCollectionName);
    }

    getMyMails(): Observable<AngularFirestoreCollection<IPreviewMail> | null> {
        return this.authService.getMyId().pipe(
            map((user: firebase.User) => {
                if (user) {
                    return this.firestore.collection<IPreviewMail>(this.mailsCollectionName, ref => {
                        return ref.where('recipients', 'array-contains', user.uid)
                        .where('type', '==', 'taken').orderBy('sendTime', 'desc');
                    });
                }
                return null;
            })
        );
    }

    getFavoritMails(): Observable<AngularFirestoreCollection<IPreviewMail> | null> {
        return this.authService.getMyId().pipe(
            map((user: firebase.User) => {
                if (user) {
                    return this.firestore.collection<IPreviewMail>(this.mailsCollectionName, ref => {
                        return ref.where('favorite', '==', true)
                        .where('owner', '==', user.uid).orderBy('sendTime', 'desc');
                    });
                }
                return null;
            })
        );
    }

    getSendedMails(): Observable<AngularFirestoreCollection<IPreviewMail> | null> {
        return this.authService.getMyId().pipe(
            map((user: firebase.User) => {
                if (user) {
                    return this.firestore.collection<IPreviewMail>(this.mailsCollectionName, ref => {
                        return ref.where('type', '==', 'sended').
                        where('owner', '==', user.uid).orderBy('sendTime', 'desc');
                    });
                }
                return null;
            })
        );
    }

    sendMail(mail: ISendingMail): Observable<any> {
        return this.authService.getMyId().pipe(
            switchMap((user) => {
                if (user) {
                    const sendingMail: IBeSendingMail = {
                        favorite: false,
                        id: this.getCreateId(),
                        sendTime: new Date(),
                        wasWatched: true,
                        owner: user.uid,
                        type: EmailTypesEnum.Sended,
                        ...mail,
                    };
                    const recipientsMails = Array<Observable<any>>();
                    for (const recipient of mail.recipients) {
                        recipientsMails.push(this.createRecipientMail(sendingMail));
                    }
                    return from(this.mailsCollection.doc(sendingMail.id).set(sendingMail))
                    .pipe(
                        concatMap(() => {
                            return forkJoin(recipientsMails);
                        })
                    );
                }
                return of(null);
            }),
        );
    }

    setFavorite(id, value): Observable<any> {
        return from(this.mailsCollection.doc(id).update({favorite: value}));
    }

    getCreateId(): string {
        return this.firestore.createId();
    }

    removeMails(mailIds: any[]): Observable<void> {
        const observables: Observable<void>[] = [];
        for (const id of mailIds) {
            observables.push(from(this.mailsCollection.doc(id).delete()));
        }
        return of().pipe(
            switchMap(() => {
                return concatMap(() => {
                    return forkJoin(observables);
                });
            })
        );
    }

    removeMail(id: string): Observable<void> {
        return from(this.mailsCollection.doc(id).delete());
    }

    setWatched(id: string): Observable<any> {
        return from(this.mailsCollection.doc(id).update({wasWatched: true}));
    }

    getMailById(id: string): Observable<any> {
        return this.mailsCollection.doc(id).valueChanges();
    }

    /** Для реализации запросов к БД либо работы с данными пользователей необходимо подключить нужно подключить firebase.admin SDK */
    getEmailByUid(uid: string): string {
        return this.uidAssociations[uid] ? this.uidAssociations[uid] : uid;
    }

    getUidByEmail(email: string): string {
        const ret = Object.keys(this.uidAssociations).find(uid => this.uidAssociations[uid] === email);
        return ret ? ret : email;
    }

    private createRecipientMail(mail: IBeSendingMail): Observable<any> {
        const newMail: IBeSendingMail = { ...mail};
        newMail.id = this.getCreateId();
        mail.type = EmailTypesEnum.Taken;
        mail.wasWatched = false;
        return from(this.mailsCollection.doc(newMail.id).set(newMail));
    }
}
