import { BeEmulationService } from './be-emulation.service';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IPreviewMail, ISendingMail } from '../../interfaces/api/api.interface';
import { switchMap } from 'rxjs/operators';

@Injectable()
export class ApiService {

  mailsCollectionName = 'mails';

  constructor(
    private beEmulationService: BeEmulationService,
  ) { }

// FB MRTHODS

  getMails(): Observable<IPreviewMail[] | null> {
    return this.beEmulationService.getMyMails()
    .pipe(
      switchMap((collection) => {
        if (collection) {
          return collection.valueChanges();
        }
        return of(null);
      })
    );
  }

  getOutboxMails(): Observable<IPreviewMail[] | null> {
    return this.beEmulationService.getSendedMails()
    .pipe(
      switchMap((collection) => {
        if (collection) {
          return collection.valueChanges();
        }
        return of(null);
      })
    );
  }

  getFavoritMails(): Observable<IPreviewMail[] | null> {
    return this.beEmulationService.getFavoritMails()
    .pipe(
      switchMap((collection) => {
        if (collection) {
          return collection.valueChanges();
        }
        return of(null);
      })
    );
  }

  getSendedMails(): Observable<IPreviewMail[] | null> {
    return this.beEmulationService.getSendedMails()
    .pipe(
      switchMap((collection) => {
        if (collection) {
          return collection.valueChanges();
        }
        return of(null);
      })
    );
  }

  getMailById(id: string): Observable<IPreviewMail> {
    return this.beEmulationService.getMailById(id);
  }

  sendMail(mail: ISendingMail): Observable<any> {
    return this.beEmulationService.sendMail(mail);
  }

  removeMails(mailIds: string[] | string): Observable<void> {
    if (typeof mailIds === 'string') {
      return this.beEmulationService.removeMail(mailIds);
    } else {
      return this.beEmulationService.removeMails(mailIds);
    }
  }

  setWatched(id: string): Observable<any> {
    return this.beEmulationService.setWatched(id);
  }

  setFavorite(id: string, value: boolean): Observable<any> {
    return this.beEmulationService.setFavorite(id, value);
  }

// HELP METHODS

  private fakeRequst<T>(data: any, timeout: number = 1000): Observable<T> {
    return new Observable<T>(subscriber => {
      setTimeout(() => {
        subscriber.next(data);
        subscriber.complete();
      }, timeout);
    });
  }
}
