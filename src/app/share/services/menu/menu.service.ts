import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class MenuService {
    type$: Subject<string>;

    constructor() {
        this.type$ = new Subject<string>();
    }

}
