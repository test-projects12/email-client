import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { from, Subscription, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { IPreviewMail } from 'src/app/share/interfaces/api/api.interface';
import { ApiService } from 'src/app/share/services/api/api.service';
import { MenuService } from 'src/app/share/services/menu/menu.service';
import { CreateMailComponent } from './modal/create-mail.component';

@UntilDestroy()
@Component({
  selector: 'app-mails-page',
  templateUrl: './mails-page.component.html',
  styleUrls: ['./mails-page.component.sass'],
})
export class MailsPageComponent implements OnInit, OnDestroy {
  previewMailData: IPreviewMail[];
  selectedMails: IPreviewMail[];
  type = 'Inbox';
  currentSubscription: any = null;

  constructor(
    private apiService: ApiService,
    private confirmationService: ConfirmationService,
    private dialogService: DialogService,
    private messageService: MessageService,
    private router: Router,
    private menuService: MenuService,
  ) {}

  ngOnInit(): void {
    this.menuService.type$.pipe(
      tap((type) => {
        if (this.currentSubscription) {
          this.currentSubscription.unsubscribe();
        }
        this.type = type;
        switch (type) {
          case 'Inbox':
            this.currentSubscription = this.getInbox();
            break;
          case 'Outbox':
            this.currentSubscription = this.getOutbox();
            break;
          case 'Favorite':
            this.currentSubscription = this.getFavorites();
            break;
        }
      }),
      untilDestroyed(this)
    ).subscribe();
    this.currentSubscription = this.getInbox();
  }

  ngOnDestroy(): void {}

  onFavorite(event, mail): void {
    event.originalEvent.stopPropagation();
    this.apiService.setFavorite(mail.id, mail.favorite).pipe(
      untilDestroyed(this)
    ).subscribe();
  }

  rowClick(mail): void {
    from(this.router.navigate(['show', mail.id])).pipe(
      catchError((error: HttpErrorResponse) => {
        const message = error.message;
        this.messageService.add({
          key: 'msg',
          severity: 'error',
          summary: 'Error',
          detail: message
        });
        return throwError(message);
      }),
      untilDestroyed(this)
    ).subscribe();
  }

  removeMails(event?: any, mail?: any): void {
    event?.stopPropagation();
    if (this.selectedMails?.length || mail) {
      this.confirmationService.confirm({
        message: `Do you want to delete ${mail ? 'this' : this.selectedMails.length} mail${mail ? '' : 's'}?`,
        header: 'Delete mails',
        icon: 'pi pi-info-circle',
        accept: () => {
          const removingIdsArray = [];
          if (mail) {
            removingIdsArray.push(mail.id);
          } else {
            for (const rMail of this.selectedMails) {
              removingIdsArray.push(rMail.id);
            }
          }
          this.apiService.removeMails(removingIdsArray);
        },
        key: 'dialog',
      });
    } else {
      this.messageService.add({
        key: 'msg',
        severity: 'warn',
        summary: 'Cant remove',
        detail: 'Please select some messages'
      });
    }
  }

  createMail(): void {
    this.dialogService.open(CreateMailComponent, {
      header: 'New mail',
      width: '70%'
    });
  }

  onCheckboxClick(event): void{
    event.stopPropagation();
  }

  private getInbox(): Subscription {
    return this.apiService
      .getMails()
      .pipe(
        tap((data: IPreviewMail[]) => {
          this.previewMailData = data;
        }),
        untilDestroyed(this)
      )
      .subscribe();
  }

  private getOutbox(): Subscription {
    return this.apiService.getOutboxMails()
    .pipe(
      tap((data: IPreviewMail[]) => {
        this.previewMailData = data;
      }),
      untilDestroyed(this)
    )
    .subscribe();
  }

  private getFavorites(): Subscription {
    return this.apiService.getFavoritMails()
    .pipe(
      tap((data: IPreviewMail[]) => {
        this.previewMailData = data;
      }),
      untilDestroyed(this)
    )
    .subscribe();
  }
}
