import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { tap } from 'rxjs/operators';
import { ISendingMail } from 'src/app/share/interfaces/api/api.interface';
import { ApiService } from 'src/app/share/services/api/api.service';
import { BeEmulationService } from 'src/app/share/services/api/be-emulation.service';

@UntilDestroy()
@Component({
    selector: 'app-create-mail',
    templateUrl: './create-mail.component.html',
})
export class CreateMailComponent implements OnInit {
    text: string;
    form: FormGroup;
    constructor(
        public apiService: ApiService,
        public ref: DynamicDialogRef,
        public messageService: MessageService,
        public config: DynamicDialogConfig,
        public beEmulationService: BeEmulationService,
    ) {}

    ngOnInit(): void {
        this.form = new FormGroup({
            adress: new FormControl(this.config.data?.recipients ? this.config.data.recipients : null, [
                Validators.required,
                Validators.email
            ]),
            theme: new FormControl(null, [
                Validators.required
            ]),
            text: new FormControl(null)
        });
    }

    submit(): void {
        if (this.form.invalid) {
            return;
        }
        const mail: ISendingMail = {
            recipients: this.asociateRecipients(this.form.value.adress),
            theme: this.form.value.theme,
            messageText: this.form.value.text
        };

        this.apiService.sendMail(mail).
        pipe(
            tap(() => {
                this.form.reset();
                this.messageService.add({
                    key: 'msg',
                    severity: 'success',
                    summary: 'Sended',
                    detail: 'Meail sended successfully'
                });
                this.ref.close();
            }),
            untilDestroyed(this)
        ).subscribe();
    }

    asociateRecipients(formRecipients: string[]): string[] {
        return formRecipients.map((recipientEmail => this.beEmulationService.getUidByEmail(recipientEmail)));
    }
}
