import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { Observable, of, throwError } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { IMail } from 'src/app/share/interfaces/api/api.interface';
import { ApiService } from 'src/app/share/services/api/api.service';
import { BeEmulationService } from 'src/app/share/services/api/be-emulation.service';
import { CreateMailComponent } from '../mails-page/modal/create-mail.component';

@UntilDestroy()
@Component({
  selector: 'app-mail-show-page',
  templateUrl: './mail-show-page.component.html',
  styleUrls: ['./mail-show-page.component.sass']
})
export class MailShowPageComponent implements OnInit {

  mail: IMail;

  constructor(
    private rout: ActivatedRoute,
    private router: Router,
    private apiService: ApiService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private dialogService: DialogService,
    private beEmulationService: BeEmulationService,
  ) { }

  ngOnInit(): void {
    this.rout.params.pipe(
      switchMap((parms: Params) => {
        return this.apiService.getMailById(parms.id).pipe(
          switchMap((mail: IMail) => {
            this.mail = mail;
            if (!mail.wasWatched) {
              return this.apiService.setWatched(parms.id);
            }
            return of(null);
          })
        );
      }),
      untilDestroyed(this)
    ).subscribe();
  }

  onFavorite(): void {
    this.apiService.setFavorite(this.mail.id, this.mail.favorite).pipe(
      catchError((error: HttpErrorResponse) => {
        const message = error.message;
        this.messageService.add({
          key: 'msg',
          severity: 'error',
          summary: 'Error',
          detail: message
        });
        return throwError(message);
      }),
      untilDestroyed(this)
    );
  }

  newMail(): void {
    this.dialogService.open(CreateMailComponent, {
      header: 'New mail',
      width: '70%'
    });
  }

  remove(): void {
    this.confirmationService.confirm({
      message: `Do you want to delete this mail?`,
      header: 'Delete mails',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.apiService.removeMails(this.mail.id).pipe(
          tap(() => {
            this.router.navigate(['/']);
          }),
          untilDestroyed(this)
        );
      },
      key: 'dialog',
    });
  }

  reply(): void {
    this.dialogService.open(CreateMailComponent, {
      header: 'New mail',
      width: '70%',
      data: {
        recipients: this.getRecipients()
      }
    });
  }

  private getRecipients(): string[] {
    if (this.mail.type === 'taken') {
      return [this.beEmulationService.getEmailByUid(this.mail.owner)];
    }
    return this.mail.recipients.map(uid => this.beEmulationService.getEmailByUid(uid));
  }
}
