import { AuthService } from './../../share/services/api/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { finalize, tap } from 'rxjs/operators';
import { IUser } from 'src/app/share/interfaces/api/api.interface';

@UntilDestroy()
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.sass']
})
export class LoginPageComponent implements OnInit {
  userName: string;
  password: string;
  form: FormGroup;
  loading: boolean;

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl(null, [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(6)
      ])
    });
  }

  submit(): void {
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    const user: IUser = {
      email: this.form.value.email,
      password: this.form.value.password
    };
    this.authService.login(user)
    .pipe(
      tap(() => {
        this.form.reset();
        this.router.navigate(['/']);
      }),
      untilDestroyed(this),
      finalize(() => {
        this.loading = false;
      })
    ).subscribe();
  }
}
