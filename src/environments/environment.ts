// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { Environment } from './firebase';

export const environment: Environment = {
  firebase : {
    apiKey: 'AIzaSyBLF2G32HB6HhvvmD2QJp5WSsjBVbN7hsA',
    authDomain: 'test-email-5fa42.firebaseapp.com',
    databaseURL: 'https://test-email-5fa42.firebaseio.com',
    projectId: 'test-email-5fa42',
    storageBucket: 'test-email-5fa42.appspot.com',
    messagingSenderId: '518595128551',
    appId: '1:518595128551:web:4b9031f4009405c3434642'
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
