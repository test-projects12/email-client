import { Environment } from './firebase';

export const environment: Environment = {
  production: true,
  firebase : {
    apiKey: 'AIzaSyBLF2G32HB6HhvvmD2QJp5WSsjBVbN7hsA',
    authDomain: 'test-email-5fa42.firebaseapp.com',
    databaseURL: 'https://test-email-5fa42.firebaseio.com',
    projectId: 'test-email-5fa42',
    storageBucket: 'test-email-5fa42.appspot.com',
    messagingSenderId: '518595128551',
    appId: '1:518595128551:web:4b9031f4009405c3434642'
  },
};
